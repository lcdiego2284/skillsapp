﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SkillsApp.Startup))]
namespace SkillsApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
